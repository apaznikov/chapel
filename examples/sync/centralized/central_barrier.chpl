use Time;
use Random;

config const numthreads   = numLocales;
config const nrepeats     = 10;
config const workload_par = 100000000;
config const seed = SeedGenerator.currentTime;

proc central_barrier()
{
  var cntr$: sync int = 1;
  var release$: single bool;

  for i in 0..numthreads - 1 {
    on Locales[i] {
      begin {
        // Do some workload
        writeln("i am ", i);
        stdout.flush();

        func();

        writeln(i, " is ready!");
        stdout.flush();

        // Decrease count of runned concurrent tasks
        var myc = cntr$;
        if myc != numthreads {
          cntr$ = myc + 1;
        } else {
          release$ = true;
        }
      }
    }
  } 

  // Wait until release = true
  release$;
  writeln("all ", numthreads, " are synchronized!");
}

proc func()
{
  var randlist = new RandomStream();
  var rand = randlist.getNext(true);

  var workload = (workload_par * rand): int;

  for i in 1..workload do
    ;
}

proc main()
{
  var timer: Timer;
  timer.start();

  for i in 1..nrepeats {
    writeln("\niter ", i);
    central_barrier();
  }

  timer.stop();
  writeln("time: ", timer.elapsed(TimeUnits.seconds) / nrepeats);
}
