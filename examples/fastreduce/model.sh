#!/bin/sh

# N="8 200 500 1000 2000 4000 8000 12000"
N="8 200 500"
nrepeats=100
# N="8 16"

for n in $N; do
  cat proto_reduce.job | \
    sed s/--n=[0-9]*/--n=$n/ | \
    sed s/--nrepeats=[0-9]*/--nrepeats=$nrepeats/ \
    >proto_reduce$n.job
  qsub proto_reduce$n.job
  rm proto_reduce$n.job
done
