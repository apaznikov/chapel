#!/bin/sh

# NLOCALES="1 2 4 6 8 10 12 14 16"
NLOCALES="1 2 4 6"
JOBTEMPL=task.torque

for nl in $NLOCALES; do
  job=reduce-nl$nl.job
  cat $JOBTEMPL | \
    sed s/%NLOCALES%/$nl/g \
    >$job
  echo "qsub $job"
  qsub $job
  rm $job
done
