#!/bin/sh

TIME_LINE="REDUCE.*time:"

NLOCALES_GREPLINE="nlocales = "
NLOCALES_XLABEL="number of locales"

N_GREPLINE="n = "
N_XLABEL="number of locales"

##
# Analyze command line options
#

[ -z "$1" ] && echo -e "./sh <type> [name]\nname: n, nl, bench" && exit

if [ "$1" == "nl" ]; then
  benchtype=micro
  grepline=$NLOCALES_GREPLINE
  xlabel=$NLOCALES_XLABEL
elif [ "$1" == "n" ]; then
  benchtype=micro
  grepline=$N_GREPLINE
  xlabel=$N_XLABEL
elif [ "$1" == "bench" ]; then
  benchtype=bench
  xlabel=$NLOCALES_XLABEL
else
  echo "Invalid type!"
  exit
fi

if [ -n "$2" ]; then
  name=$2
else
  name=fastreduce
fi

datafile=dat
echo -e "nl\ttime, s" >$datafile

##
# Analyze output files
#

if [ "$benchtype" == "bench" ]; then
  #
  # Benchmarks (HPL, ptrans, stream, ...)
  #
  for timefile in `ls | grep exectime`; do
    exectime=`cat $timefile`
    nlocales=`echo $timefile | sed s/.*nl//`
    echo -e "$nlocales\t$exectime" >>$datafile
  done
  cat $datafile | sort -n >$datafile.new
  mv $datafile.new $datafile
else
  #
  # Microbenchmarks (reduce only)
  #
  for ofile in `ls | egrep "\.o.*"`; do
    nlocales=`cat $ofile | egrep "$grepline" \
                         | sed s/"$grepline"//`

    exectime=`cat $ofile | egrep $TIME_LINE \
                         | sed s/"$TIME_LINE\ "// \
                         | sed s/\ sec//`

    echo -e "$nlocales\t$exectime" >>$datafile

  done
fi

##
# Gnuplot
#

cat gp | sed s/%XLABEL%/"$xlabel"/ | sed s/%NAME%/"$name"/ >$name.gp
gnuplot $name.gp
