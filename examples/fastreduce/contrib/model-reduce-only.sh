#!/bin/sh

# N="8 200 500 1000 2000 4000 8000 12000"
N="2000 4000 6000 8000 10000 12000 14000 16000 18000 20000 22000 24000 26000 28000 30000"
nrepeats=1
JOBTEMPL=task.torque
# N="8 16"

for n in $N; do
  job=reduce-n$n.job
  cat $JOBTEMPL | \
    sed s/--n=[0-9]*/--n=$n/ | \
    sed s/--nrepeats=[0-9]*/--nrepeats=$nrepeats/ \
    >$job
  echo "qsub $job"
  qsub $job
  rm $job
done
