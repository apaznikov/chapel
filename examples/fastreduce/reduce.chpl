use BlockDist;
/*use CyclicDist;*/
use Time;

config const n = 8;
config const nrepeats = 100;
config type arrtype = real;

proc main {
  //
  // Initialization
  //

  var timer_begin: Timer;
  timer_begin.start();

  var Space = {1..n, 1..n};
  var BlockSpace = Space dmapped Block(boundingBox=Space);
  /*var BlockSpace2 = Space dmapped Cyclic(startIdx=Space.low);*/
  var BA: [BlockSpace] arrtype;
  /*var BA2: [BlockSpace2] arrtype;*/

  /*if isBlockDistDom(BlockSpace2) {*/
  /*  writeln("block");*/
  /*} else {*/
  /*  writeln("not block");*/
  /*}*/

  forall (i, j) in Space do
    BA[i, j] = i * 10 + j;

  /*forall (i, j) in Space do*/
  /*  BA2[i, j] = i * 10 + j;*/

  writeln("nlocales = ", numLocales);
  /*writeln("nthreads = ", numThreads);*/
  writeln("nrepeats = ", nrepeats);
  writeln("n = ", n);

  //writeln(BA);

  timer_begin.stop();

  writeln("\nINIT ", timer_begin.elapsed(TimeUnits.seconds), "\n");

  //
  // Reduction
  //

  var timer: Timer;
  timer.start();

  // Do 'nrepeats' measurements
  for it in 1..nrepeats {
    //var timer_it: Timer;
    //timer_it.start();

    var sum = + reduce BA;
    /*var sum2 = + reduce BA2;*/
 
    //timer_it.stop();

    //writeln("it ", it, ": ", timer_it.elapsed(TimeUnits.seconds));
    writeln("GLOBAL REDUCE: ", sum);
    /*writeln("GLOBAL REDUCE CYCLIC: ", sum2);*/
  }

  timer.stop();

  writeln("\n============================");
  writeln("REDUCE time: ", 
          timer.elapsed(TimeUnits.seconds) / nrepeats, " sec");
}
