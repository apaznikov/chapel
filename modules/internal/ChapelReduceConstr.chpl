use ChapelReduce;

proc getReduceScanOp(op: SumReduceScanOp, type t) {
  return new SumReduceScanOp(eltType = t);
}

proc getReduceScanOp(op: SumReduceScanOp, type t, space: domain) {
  var opArr: [space] SumReduceScanOp(eltType = t);
  coforall i in space {
    on opArr[i] {
      opArr[i] = new SumReduceScanOp(eltType = t);
    }
  }
  return opArr;
}

proc getReduceScanOp(op: ProductReduceScanOp, type t) {
  return new ProductReduceScanOp(eltType = t);
}

proc getReduceScanOp(op: ProductReduceScanOp, type t, space: domain) {
  var opArr: [space] ProductReduceScanOp(eltType = t);
  coforall i in space {
    on opArr[i] {
      opArr[i] = new ProductReduceScanOp(eltType = t);
    }
  }
  return opArr;
}

proc getReduceScanOp(op: MaxReduceScanOp, type t) {
  return new MaxReduceScanOp(eltType = t);
}

proc getReduceScanOp(op: MaxReduceScanOp, type t, space: domain) {
  var opArr: [space] MaxReduceScanOp(eltType = t);
  coforall i in space {
    on opArr[i] {
      op[i] = new MaxReduceScanOp(eltType = t);
    }
  }
  return op;
}

proc getReduceScanOp(op: MinReduceScanOp, type t) {
  return new MinReduceScanOp(eltType = t);
}

proc getReduceScanOp(op: MinReduceScanOp, type t, space: domain) {
  var opArr: [space] MinReduceScanOp(eltType = t);
  coforall i in space {
    on opArr[i] {
      opArr[i] = new MinReduceScanOp(eltType = t);
    }
  }
  return opArr;
}

proc getReduceScanOp(op: LogicalAndReduceScanOp, type t) {
  return new LogicalAndReduceScanOp(eltType = t);
}

proc getReduceScanOp(op: LogicalAndReduceScanOp, type t, space: domain) {
  var opArr: [space] LogicalAndReduceScanOp(eltType = t);
  coforall i in space {
    on opArr[i] {
      opArr[i] = new LogicalAndReduceScanOp(eltType = t);
    }
  }
  return opArr;
}

proc getReduceScanOp(op: LogicalOrReduceScanOp, type t) {
  return new LogicalOrReduceScanOp(eltType = t);
}

proc getReduceScanOp(op: LogicalOrReduceScanOp, type t, space: domain) {
  var opArr: [space] LogicalOrReduceScanOp(eltType = t);
  coforall i in space {
    on opArr[i] {
      opArr[i] = new LogicalOrReduceScanOp(eltType = t);
    }
  }
  return opArr;
}

proc getReduceScanOp(op: BitwiseAndReduceScanOp, type t) {
  return new BitwiseAndReduceScanOp(eltType = t);
}

proc getReduceScanOp(op: BitwiseAndReduceScanOp, type t, space: domain) {
  var opArr: [space] BitwiseAndReduceScanOp(eltType = t);
  coforall i in space {
    on opArr[i] {
      opArr[i] = new BitwiseAndReduceScanOp(eltType = t);
    }
  }
  return opArr;
}

proc getReduceScanOp(op: BitwiseOrReduceScanOp, type t) {
  return new BitwiseOrReduceScanOp(eltType = t);
}

proc getReduceScanOp(op: BitwiseOrReduceScanOp, type t, space: domain) {
  var opArr: [space] BitwiseOrReduceScanOp(eltType = t);
  coforall i in space {
    on opArr[i] {
      opArr[i] = new BitwiseOrReduceScanOp(eltType = t);
    }
  }
  return opArr;
}

proc getReduceScanOp(op: BitwiseXorReduceScanOp, type t) {
  return new BitwiseXorReduceScanOp(eltType = t);
}

proc getReduceScanOp(op: BitwiseXorReduceScanOp, type t, space: domain) {
  var opArr: [space] BitwiseXorReduceScanOp(eltType = t);
  coforall i in space {
    on opArr[i] {
      opArr[i] = new BitwiseXorReduceScanOp(eltType = t);
    }
  }
  return opArr;
}

proc getReduceScanOp(op: maxloc, type t) {
  return new maxloc(eltType = t);
}

proc getReduceScanOp(op: maxloc, type t, space: domain) {
  var opArr: [space] maxloc(eltType = t);
  coforall i in space {
    on opArr[i] {
      opArr[i] = new maxloc(eltType = t);
    }
  }
  return opArr;
}

proc getReduceScanOp(op: minloc, type t) {
  return new minloc(eltType = t);
}

proc getReduceScanOp(op: minloc, type t, space: domain) {
  var opArr: [space] minloc(eltType = t);
  coforall i in space {
    on opArr[i] {
      opArr[i] = new minloc(eltType = t);
    }
  }
  return opArr;
}
