use BlockDist;
use Time;
use ChapelReduceConstr;

config const numThreads = here.numCores;
config type arrtype = real;

// fastReduceBlocks: Parallel reduce implementation using splitDomainBlocks
proc fastReduceBlocks(ref BA, ref op, type eltType)
{
  var timer: Timer;
  timer.start();

  var BlockSpace = BA.domain;
  var globReduce = getReduceScanOp(op, eltType);

  var distLocDom = BlockSpace.getTargetLocDom 
                   dmapped Block(boundingBox = BlockSpace.getTargetLocDom);
  var locReduce = getReduceScanOp(op, eltType, distLocDom);

  timer.stop();
  writeln("init time: ", timer.elapsed(TimeUnits.seconds));

  var timer2: Timer;
  timer2.start();

  // Parallelize by locales
  coforall locid in distLocDom {
    
    on BlockSpace.getTargetLocales(locid) {
      var locBlock = BlockSpace.getMyBlock(locid);

      // Parallelize by threads

      // Split domain by blocks
      var threadBlocks : [1..numThreads] domain(locBlock.rank);
      threadBlocks = splitDomainBlocks(locBlock, numThreads);

      var threadDomain = {1..numThreads};
      var threadReduce = getReduceScanOp(op, eltType, threadDomain);

      coforall tid in 1..numThreads {
        /*var rank = locBlock.rank;*/

        for elem in threadBlocks[tid] {
          threadReduce[tid].accumulate(BA.getMyLocArr[elem]);
        }
      }

      // Combine results from all threads
      for tid in 1..numThreads {
        locReduce[locid].combine(threadReduce[tid]);
      }
    }
  }

  timer2.stop();
  writeln("middle (main comp): ", timer2.elapsed(TimeUnits.seconds));

  var timer3: Timer;
  timer3.start();

  // Serial computation for final result by combining all locales' results
  for locid in distLocDom {
    globReduce.combine(locReduce[locid]);
  }

  timer3.stop();
  writeln("last (serial) loop: ", timer3.elapsed(TimeUnits.seconds));

  return globReduce.value;
  
  /*
  //
  // Binary tree combination
  //

  var locDomLinear = {1..BlockSpace.getTargetLocDom.size};
  var distLocDomLinear = locDomLinear dmapped Block(boundingBox=locDomLinear);

  var newIndexes: [locDomLinear] BlockSpace.getTargetLocDom.rank * int;

  // Serialize distLocDom
  for (distlocid, serlocid) in zip(distLocDom, locDomLinear) do
    newIndexes[serlocid] = distlocid;

  //for locid in locDomLinear do
    //writeln("locid = ", newIndexes[locid]);

  var indCntr = BlockSpace.getTargetLocDom.size;

  while (indCntr > 1) {

    var indexes: 
      [locDomLinear] BlockSpace.getTargetLocDom.rank * int = newIndexes;

    var numElems = indCntr;
    var odd_flag: int = 0;

    indCntr = 0;
    if numElems % 2 != 0 {
      //writeln("numElem = ", numElems, "!");
      odd_flag = 1;
      newIndexes[numElems / 2 + 1] = indexes[numElems];
      numElems -= 1;
    } 

    var numPairs$: sync int = numElems / 2;
    var release$: single bool;

    var i = 1;

    do {
      var first = indexes[i];
      var second = indexes[i + 1];

      begin {
        on locReduce[first] {
          locReduce[first].combine(locReduce[second]);
          //writeln("combine(", first, ",", second, ")");
        }

        // Decrease count of runned concurrent tasks
        var myc = numPairs$;
        if myc != 1 {
          numPairs$ = myc - 1;
        } else {
          release$ = true;
        }
      }

      indCntr += 1;

      newIndexes[indCntr] = first;
      i += 2;
    } while i + 1 <= numElems;

    indCntr += odd_flag;

    // Barrier synchronization
    release$;
  }

  return locReduce[newIndexes[1]].value;
  */
}

// FastReduce: fast reduce prototype
class FastReduce {
  var value: arrtype;

  inline proc accumulate(x: arrtype) {
    value = value + x;
  }

  inline proc combine(locOp) {
    value = value + locOp.value;
  }
}

// fastReduceSimple: Parallel reduce implementation using splitDomainSimple
proc fastReduceSimple(ref BA)
{
  var globReduce = new FastReduce(value = 0);
  var BlockSpace = BA.domain;
  var locReduce: [BlockSpace.getTargetLocDom] FastReduce;

  // Allocate as global variable
  for locid in BlockSpace.getTargetLocDom do
    locReduce[locid] = new FastReduce(value = 0);

  // Parallelize by locales
  coforall locid in BlockSpace.getTargetLocDom {
    
    on BlockSpace.getTargetLocales(locid) {
      var locBlock = BlockSpace.getMyBlock(locid);

      writeln("Locale ", locid, " : ", locBlock, "\n", BA[locBlock], "\n");

      // Parallelize by threads
      var bounds = splitDomainSimple(locBlock, numThreads);
      var threadReduce: [1..numThreads] FastReduce;

      coforall tid in 1..numThreads {
        threadReduce[tid] = new FastReduce(value = 0);

        var rank = locBlock.rank;

        // Current index for bypass all thread elements
        var curr_ind = bounds[tid].low;

        while curr_ind != bounds[tid].high {
          threadReduce[tid].accumulate(BA[curr_ind]);

          // increment right dimension index
          curr_ind[rank] += 1;

          // check all dimension bounds in loop
          for dim in 1..locBlock.rank by -1 {
            if (curr_ind[dim] > locBlock.dim(dim).high) {
              curr_ind[dim] = locBlock.dim(dim).low;
              curr_ind[dim - 1] += 1;
            }
          }
        }

        threadReduce[tid].accumulate(BA[bounds[tid].high]);
      }

      // Combine results from all threads
      for tid in 1..numThreads {
        /*writeln("it ", it, " summarizing ", tid, " ", threadReduce[tid].value);*/
        locReduce[locid].combine(threadReduce[tid]);
      }
    }
  }

  // Serial computation for final result by combining all locales' results
  for locid in BlockSpace.getTargetLocDom {
    globReduce.combine(locReduce[locid]);
  }

  return globReduce.value;
}

// Split rectangular domain into rectangular blocks. Return domain.
proc splitDomainBlocks(ref boundingBox: domain, nblocks: int) {

  var rank = boundingBox.rank;

  // Find the longest dimension

  var longest_dim = 1;
  var max_length = boundingBox.dim(1).length;

  for dim in 2..rank {
    if (boundingBox.dim(dim).length > max_length) {
      longest_dim = dim;
      max_length = boundingBox.dim(dim).length;
    }
  }

  // Devide the longest dimension

  var div = boundingBox.dim(longest_dim).length / nblocks;
  var mod = boundingBox.dim(longest_dim).length % nblocks;
  var chunksize: [1..nblocks] int;

  // TODO test if dimension is too small
  // Fill chunk sizes uniformly 
  for iblock in 1..mod do
    chunksize[iblock] = div + 1;

  for iblock in mod+1..nblocks do
    chunksize[iblock] = div;

  // Make resulting domain chunks
  var blocks: [1..nblocks] domain(boundingBox.rank);
  var low_bound = boundingBox.dim(longest_dim).low - 1;
  var high_bound = low_bound;

  for iblock in 1..nblocks {
    blocks[iblock] = boundingBox;

    if chunksize[iblock] == 0 {
      low_bound = 0;
      high_bound = 0;
    } else {
      low_bound = high_bound + 1;
      high_bound += chunksize[iblock];
    }

    blocks[iblock] = boundingBox.cutrange(longest_dim, low_bound..high_bound);
  }

  return blocks;
}

// High and low bounds of block within the domain
class chunkBounds {
  type index_t;
  var low: index_t;
  var high: index_t;
}

// Split domain into arbitrary number of chunks. Return bounds of chunks.
proc splitDomainSimple(ref boundingBox: domain, nblocks: int) {

  var rank = boundingBox.rank;
  var bounds: [1..nblocks] chunkBounds(index_t = index(boundingBox));
  var div = boundingBox.size / nblocks;
  var mod = boundingBox.size % nblocks;
  var chunksize: [1..nblocks] int;
  var dimweight: [1..rank] int;

  for iblock in 1..nblocks do
    bounds[iblock] = new chunkBounds(index_t = index(boundingBox));

  // Fill chunk sizes for even number of elements
  // NOTE Possibly uniformity is not reasonable?
  for iblock in 1..mod do
    chunksize[iblock] = div + 1;

  for iblock in mod+1..nblocks do
    chunksize[iblock] = div;

  // Weigth is number of right-dimension elements per other-dimension elements
  var currweight = 1;
  for dim in 2..rank by -1 {
    dimweight[dim] = currweight;
    currweight *= boundingBox.dim(dim).size;
  }
  dimweight[1] = currweight;

  var high = -1, low = 0: int;
  var high_rest = 0, low_rest = 0: int;
  var dimsize_low = 0, dimsize_high = 0;

  // Compute corrdinates for each begin (low) and end (high) of a chunk 
  for iblock in 1..nblocks {
    low = high + 1;
    high += chunksize[iblock];

    low_rest = low;
    high_rest = high;

    for dim in 1..rank {
      dimsize_low = low_rest / dimweight[dim];
      dimsize_high = high_rest / dimweight[dim];

      low_rest -= dimsize_low * dimweight[dim];
      high_rest -= dimsize_high * dimweight[dim];

      bounds[iblock].low[dim] = dimsize_low + boundingBox.dim(dim).low;
      bounds[iblock].high[dim] = dimsize_high + boundingBox.dim(dim).low;
    }
  }

  return bounds;
}
